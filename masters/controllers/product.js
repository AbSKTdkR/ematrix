const masterModel = require('../models/dao/master');
const response = require('../../config/responses');
const { validationHandler } = require('../../config/validation-handler');

module.exports.fetchProduct = async (req, res) => {
    try {
        let data = await masterModel.fetchProduct(req.dao_obj);
        if (data.result) {
            let count_data = await masterModel.countAllProduct(req.dao_obj);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.PRODUCT_NOT_FOUND });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
};

module.exports.fetchSpecificProduct = async (req, res) => {
    try {
        let condition = { _id: req.params.id }
        let data = await masterModel.fetchProduct({ filter: condition });
        if (data.result) {
            let count_data = await masterModel.countAllProduct(condition);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.PRODUCT_NOT_FOUND });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.saveProduct = async (req, res, next) => {
    req.getValidationResult().then(validationHandler()).then(async () => {
        try {
            let productData = await masterModel.fetchProduct({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false } });
            if (productData.result) {
                res.status(200).json({ "error": response.error.PRODUCT_EXISTS });
            } else {
                let productData = await masterModel.addProduct(req.body);
                if (productData.result) {
                    res.status(200).json({ "message": response.success.PRODUCT_SAVED.message, result: productData.result });
                } else {
                    res.status(200).json({ error: response.error.PRODUCT_NOT_FOUND });
                }
            }
        } catch (error) {
            res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
        }
    }).catch(next => {
        res.status(500).json({ error: next.toString() });
    });
}

module.exports.updateProduct = async (req, res) => {
    try {
        let productData = await masterModel.fetchProduct({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false, _id: { $ne: req.params.id } } })
        if (productData.result) {
            res.status(200).json({ "error": response.error.PRODUCT_EXISTS });
        } else {
            let condition = { _id: req.params.id }
            let productData = await masterModel.updateProduct(condition, req.body);
            if (productData.result) {
                res.status(200).json({ "message": response.success.CATEGORY_UPDATED.message, result: productData.result });
            } else {
                res.status(200).json({ error: response.error.PRODUCT_NOT_UPDATED });
            }
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.deleteProduct = async (req, res) => {
    try {
        let condition = { _id: req.params.id };
        let productData = await masterModel.deleteProduct(condition);
        if (productData.result) {
            res.status(200).json({ "message": response.success.PRODUCT_DELETED.message, result: productData.result });
        } else {
            res.status(200).json({ error: response.error.PRODUCT_NOT_DELETED });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}