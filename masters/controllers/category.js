const masterModel = require('../models/dao/master');
const response = require('../../config/responses');
const { validationHandler } = require('../../config/validation-handler');

module.exports.fetchCategory = async (req, res) => {
    try {
        let data = await masterModel.fetchCategory(req.dao_obj);
        if (data.result) {
            let count_data = await masterModel.countAllCategory(req.dao_obj);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.CATEGORY_NOT_FOUND });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
};

module.exports.fetchSpecificCategory = async (req, res) => {
    try {
        let condition = { _id: req.params.id }
        let data = await masterModel.fetchCategory({ filter: condition });
        if (data.result) {
            let count_data = await masterModel.countAllCategory(condition);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.CATEGORY_NOT_FOUND });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.saveCategory = async (req, res, next) => {
    req.getValidationResult().then(validationHandler()).then(async () => {
        try {
            let categoryData = await masterModel.fetchCategory({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false } });
            if (categoryData.result) {
                res.status(200).json({ "error": response.error.CATEGORY_EXISTS });
            } else {
                let categoryData = await masterModel.addCategory(req.body);
                if (categoryData.result) {
                    res.status(200).json({ "message": response.success.CATEGORY_SAVED.message, result: categoryData.result });
                } else {
                    res.status(200).json({ error: response.error.CATEGORY_NOT_SAVED });
                }
            }
        } catch (error) {
            res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
        }
    }).catch(next => {
        res.status(500).json({ error: next.toString() });
    });
}

module.exports.updateCategory = async (req, res) => {
    try {
        let categoryData = await masterModel.fetchCategory({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false, _id: { $ne: req.params.id } } })
        if (categoryData.result) {
            res.status(200).json({ "error": response.error.CATEGORY_EXISTS });
        } else {
            let condition = { _id: req.params.id }
            let categoryData = await masterModel.updateCategory(condition, req.body);
            if (categoryData.result) {
                res.status(200).json({ "message": response.success.CATEGORY_UPDATED.message, result: categoryData.result });
            } else {
                res.status(200).json({ error: response.error.CATEGORY_NOT_UPDATED });
            }
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.deleteCategory = async (req, res) => {
    try {
        let condition = { _id: req.params.id };
        let categoryData = await masterModel.deleteCategory(condition);
        if (categoryData.result) {
            res.status(200).json({ "message": response.success.CATEGORY_DELETED.message, result: categoryData.result });
        } else {
            res.status(200).json({ error: response.error.CATEGORY_NOT_DELETED });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}