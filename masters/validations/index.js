const response = require('../../config/responses');
const { body } = require('express-validator/check');


module.exports.validateCategory = () => {
    return [
        body('name', response.error.NAME_REQUIRED.message).exists().not().isEmpty()
    ]
}
module.exports.validateProduct = () => {
    return [
        body('name', response.error.NAME_REQUIRED.message).exists().not().isEmpty()
    ]
}