const express = require('express');
const router = express.Router();
const categoryController = require('../controllers/category');
const productController = require('../controllers/product');
const validation = require('../validations');

router.post("/category", validation.validateCategory(), categoryController.saveCategory);
router.get("/category", categoryController.fetchCategory);
router.get("/category/:id", categoryController.fetchSpecificCategory);
router.put("/category/:id", categoryController.updateCategory);
router.delete("/category/:id", categoryController.deleteCategory);

router.post("/product", validation.validateProduct(), productController.saveProduct);
router.get("/product", productController.fetchProduct);
router.get("/product/:id", productController.fetchSpecificProduct);
router.put("/product/:id", productController.updateProduct);
router.delete("/product/:id", productController.deleteProduct);

module.exports = router;