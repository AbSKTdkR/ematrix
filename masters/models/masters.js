const mongoose = require('mongoose');
const schema = mongoose.Schema;

const categorySchema = new mongoose.Schema({
    name: { type: String, required: true },
    sub_categories: [{ type: schema.Types.ObjectId, ref: 'categories', default: null }],
    products: [{ type: schema.Types.ObjectId, ref: 'products', default: null }],
});
const categoryDetails = mongoose.model('categories', categorySchema);

const productSchema = new mongoose.Schema({
    name: { type: String, required: true },
    price: { type: Number, default: 0 },
    categories: [{ type: schema.Types.ObjectId, ref: 'categories', default: null }]
});
const productDetails = mongoose.model('products', productSchema);

module.exports = {
    category: categoryDetails,
    product:productDetails
}