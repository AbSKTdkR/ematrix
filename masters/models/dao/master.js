const model = require('../masters');
const schemaConstants = require('../../../config/schema');

module.exports.fetchCategory = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.category.find(dao_obj.filter)
		.populate({ path: "sub_categories", select: schemaConstants.category })
		.populate({ path: "products", select: schemaConstants.product })
		.skip(dao_obj.skip).limit(dao_obj.limit).sort(dao_obj.sort).exec().then((data) => {
			if (data.length > 0)
				return { result: data };
			else
				return { error: "Category Data Not Found" };
		});
};

module.exports.countAllCategory = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.category.find(dao_obj.filter).count().exec().then((data) => {
		if (data) {
			return { result: data };
		} else {
			return { error: "Data not found" };
		}
	})
};

module.exports.addCategory = (data) => {
	let categoryData = model.category(data);
	return categoryData.save().then(data => {
		return ({ result: data });
	})
}

module.exports.updateCategory = async (condition, data, multi = false) => {
	try {
		let result = await model.category.update(condition, data, { multi: multi });
		return ({ result: result })
	} catch (error) {
		return ({ error: error })
	}
}

module.exports.deleteCategory = async (condition) => {
	let data = await model.category.update(condition, { deleted: true });
	if (data.nModified === 1) {
		return { result: data };
	} else {
		return { error: "Category not found" };
	}
}

module.exports.fetchProduct = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.product.find(dao_obj.filter)
		.populate({ path: "categories", select: schemaConstants.category })
		.skip(dao_obj.skip).limit(dao_obj.limit).sort(dao_obj.sort).exec().then((data) => {
			if (data.length > 0)
				return { result: data };
			else
				return { error: "Product Data Not Found" };
		});
};

module.exports.countAllProduct = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.product.find(dao_obj.filter).count().exec().then((data) => {
		if (data) {
			return { result: data };
		} else {
			return { error: "Data not found" };
		}
	})
};

module.exports.addProduct = (data) => {
	let productData = model.product(data);
	return productData.save().then(data => {
		return ({ result: data });
	})
}

module.exports.updateProduct = async (condition, data, multi = false) => {
	try {
		let result = await model.product.update(condition, data, { multi: multi });
		return ({ result: result })
	} catch (error) {
		return ({ error: error })
	}
}

module.exports.deleteProduct = async (condition) => {
	let data = await model.product.update(condition, { deleted: true });
	if (data.nModified === 1) {
		return { result: data };
	} else {
		return { error: "Product not found" };
	}
}