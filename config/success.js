module.exports = {
"CATEGORY_SAVED": {
    "code": 0,
    "message": "Category saved successfully."
},
"CATEGORY_UPDATED":{
    "code": 1,
    "message": "Category updated successfully"
},
"CATEGORY_DELETED":{
    "code": 2,
    "message": "Category deleted successfully."
},
"PRODUCT_SAVED": {
    "code": 3,
    "message": "Product saved successfully."
},
"PRODUCT_UPDATED":{
    "code": 4,
    "message": "Product updated successfully"
},
"PRODUCT_DELETED":{
    "code": 5,
    "message": "Product deleted successfully."
}
}