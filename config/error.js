module.exports = {
    "DEFAULT_MONGOOSE_ERROR": {
        "code": 0,
        "title": "Database error",
        "message": "Error occurred while connecting to database",
        "resolution": "Contact Adminstrator"
    },
    "CATEGORY_NOT_FOUND": {
        "code": 1,
        "message": "Category not found",
        "resolution": "Category not found. Enter new data"
    },
    "NAME_REQUIRED": {
        "code": 2,
        "message": "Name is empty",
        "resolution": "Please enter Name"
    },
    "ROLE_REQUIRED": {
        "code": 3,
        "message": "Role is empty",
        "resolution": "Please enter Role"
    },
    "CATEGORY_NOT_SAVED":{
        "code": 4,
        "message": "Category data not saved.",
        "resolution": "Category data not saved. Please contact administrator"
    },
    "CATEGORY_EXISTS":{
        "code": 5,
        "message": "Category Already Exists",
        "resolution": "Category already exists. Please enter new category"
    },
    "CATEGORY_NOT_UPDATED":{
        "code": 6,
        "message": "Category not updated.",
        "resolution": "Category not updated. Please contact administrator"
    },
    "CATEGORY_REQUIRED":{
        "code": 7,
        "message": "Category is empty",
        "resolution": "Please enter Category"
    },
    "PRODUCT_EXISTS":{
        "code": 8,
        "message": "Product Already Exists",
        "resolution": "Product already exists. Please enter new product"
    },
    "PRODUCT_NOT_FOUND": {
        "code": 9,
        "message": "Product not found",
        "resolution": "Product not found. Enter new data"
    },
    "PRODUCT_NOT_UPDATED":{
        "code": 10,
        "message": "Product not updated.",
        "resolution": "Product not updated. Please contact administrator"
    },
    "CATEGORY_NOT_DELETED": {
        "code": 11,
        "message": "Category not deleted.",
        "resolution": "Category not deleted. Please contact administrator"
    },
    "PRODUCT_NOT_DELETED": {
        "code": 12,
        "message": "Product not deleted.",
        "resolution": "Product not deleted. Please contact administrator"
    },
    "USER_NOT_FOUND": {
        "code": 13,
        "message": "User not found",
        "resolution": "User not found. Enter new data"
    },
    "INVALID_USERNAME": {
        "code": 14,
        "message": "Username is invalid",
        "resolution": "Enter valid username"
    },
    "NO_ACCESS": {
        "code": 15,
        "message": "Permission Denied!",
        "resolution": "Please contact administrator"
    },
    "ROLE_NOT_FOUND":{
        "code": 16,
        "message": "Role not found!",
        "resolution": "Please contact administrator"
    }
}
