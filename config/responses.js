const errorFile = require('../config/error')
const successFile = require('../config/success')

module.exports = {
  error: errorFile,
  success: successFile,
  getResponse: (code) => {
    // returns node promise and internally handles mongoose's promise,
    // i.e. promise inside promise
    const promise = new Promise((resolve, reject) => {
      let query = responses.find({}, { "_id": 0 });
      if (code) {
        query = responses.findOne({
          'code': code
        }, { "_id": 0 });
      }
      query.exec().then((errorNode) => {
        resolve(errorNode);
      })
        .catch((error) => {
          resolve(error);
        });
    });

    return promise;
  },
  globalBussinessErrorHandler: (errorCode, res) => {
    Error(errorCode).then((error) => {
      logger.writeError('error: ' + error);
      return res.status(500).json({ status: false, error: error });
    });
  },
  mongooseErrorWrapper: function (err) {
    if (process.env.NODE_ENV !== 'production') {
    }
    return DEFAULT_MONGOOSE_ERROR;
  }
}
