module.exports.validationHandler = next => result => {
    if (result.isEmpty()) return
    if (!next) {
        if (result.array().length > 0) {
            throw new Error(
                result.array().map(i => ` ${i.msg}`).join(', ')
            )
        }
    }
    else {
        return next(
            new Error(
                result.array().map(i => ` ${i.msg}`).join(', ')
            )
        )
    }
}