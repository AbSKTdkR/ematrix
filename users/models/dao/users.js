const model = require('../users');
const schemaConstants = require('../../../config/schema');

module.exports.fetchUser = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.user.find(dao_obj.filter)
		.populate({ path: "sub_categories", select: schemaConstants.user })
		.populate({ path: "products", select: schemaConstants.product })
		.skip(dao_obj.skip).limit(dao_obj.limit).sort(dao_obj.sort).exec().then((data) => {
			if (data.length > 0)
				return { result: data };
			else
				return { error: "User Data Not Found" };
		});
};

module.exports.countAllUser = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.user.find(dao_obj.filter).count().exec().then((data) => {
		if (data) {
			return { result: data };
		} else {
			return { error: "Data not found" };
		}
	})
};

module.exports.addUser = (data) => {
	let userData = model.user(data);
	return userData.save().then(data => {
		return ({ result: data });
	})
}

module.exports.updateUser = async (condition, data, multi = false) => {
	try {
		let result = await model.user.update(condition, data, { multi: multi });
		return ({ result: result })
	} catch (error) {
		return ({ error: error })
	}
}

module.exports.deleteUser = async (condition) => {
	let data = await model.user.update(condition, { deleted: true });
	if (data.nModified === 1) {
		return { result: data };
	} else {
		return { error: "User not found" };
	}
}

module.exports.fetchProduct = (dao_obj) => {
	if (dao_obj && dao_obj.filter && Object.entries(dao_obj.filter).length === 0)
		dao_obj.filter = { deleted: false };
	return model.product.find(dao_obj.filter)
		.populate({ path: "categories", select: schemaConstants.user })
		.skip(dao_obj.skip).limit(dao_obj.limit).sort(dao_obj.sort).exec().then((data) => {
			if (data.length > 0)
				return { result: data };
			else
				return { error: "Product Data Not Found" };
		});
};