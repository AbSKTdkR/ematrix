const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    role: { type: String, required: true },
    deleted: { type: Boolean, default: false }
});
const userDetails = mongoose.model('users', userSchema);

module.exports = {
    user: userDetails
}