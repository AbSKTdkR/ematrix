const express = require('express');
const router = express.Router();
const userController = require('../controllers/users');
const validation = require('../validations');

router.post("/", validation.validateUser(), userController.saveUser);
router.get("/", userController.fetchUser);
router.get("/:id", userController.fetchSpecificUser);
router.put("/:id", userController.updateUser);
router.delete("/:id", userController.deleteUser);

module.exports = router;