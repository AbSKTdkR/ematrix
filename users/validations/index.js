const response = require('../../config/responses');
const { body } = require('express-validator/check');


module.exports.validateUser = () => {
    return [
        body('name', response.error.NAME_REQUIRED.message).exists().not().isEmpty(),
        body('role', response.error.ROLE_REQUIRED.message).exists().not().isEmpty()
    ]
}