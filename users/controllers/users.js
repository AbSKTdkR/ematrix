const masterModel = require('../models/dao/users');
const response = require('../../config/responses');
const { validationHandler } = require('../../config/validation-handler');

module.exports.fetchUser = async (req, res) => {
    try {
        let data = await masterModel.fetchUser(req.dao_obj);
        if (data.result) {
            let count_data = await masterModel.countAllUser(req.dao_obj);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.USER_NOT_FOUND });
        }
    } catch (error) {
        console.log("error",error);
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
};

module.exports.fetchSpecificUser = async (req, res) => {
    try {
        let condition = { _id: req.params.id }
        let data = await masterModel.fetchUser({ filter: condition });
        if (data.result) {
            let count_data = await masterModel.countAllUser(condition);
            res.status(200).json({ result: data.result, total_count: count_data.result });
        } else {
            res.status(200).json({ error: response.error.USER_NOT_FOUND });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.saveUser = async (req, res, next) => {
    req.getValidationResult().then(validationHandler()).then(async () => {
        try {
            let userData = await masterModel.fetchUser({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false } });
            if (userData.result) {
                res.status(200).json({ "error": response.error.USER_EXISTS });
            } else {
                let userData = await masterModel.addUser(req.body);
                if (userData.result) {
                    res.status(200).json({ "message": response.success.USER_SAVED.message, result: userData.result });
                } else {
                    res.status(200).json({ error: response.error.USER_NOT_SAVED });
                }
            }
        } catch (error) {
            res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
        }
    }).catch(next => {
        res.status(500).json({ error: next.toString() });
    });
}

module.exports.updateUser = async (req, res) => {
    try {
        let userData = await masterModel.fetchUser({ filter: { 'name': { "$regex": req.body.name, "$options": "i" }, deleted: false, _id: { $ne: req.params.id } } })
        if (userData.result) {
            res.status(200).json({ "error": response.error.USER_EXISTS });
        } else {
            let condition = { _id: req.params.id }
            let userData = await masterModel.updateUser(condition, req.body);
            if (userData.result) {
                res.status(200).json({ "message": response.success.USER_UPDATED.message, result: userData.result });
            } else {
                res.status(200).json({ error: response.error.USER_NOT_UPDATED });
            }
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}

module.exports.deleteUser = async (req, res) => {
    try {
        let condition = { _id: req.params.id };
        let userData = await masterModel.deleteUser(condition);
        if (userData.result) {
            res.status(200).json({ "message": response.success.USER_DELETED.message, result: userData.result });
        } else {
            res.status(200).json({ error: response.error.USER_NOT_DELETED });
        }
    } catch (error) {
        res.status(500).json({ error: response.error.DEFAULT_MONGOOSE_ERROR, dev_err: error });
    }
}