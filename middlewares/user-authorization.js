const userModel = require('../users/models/users');
const response = require('../config/responses');

module.exports = (options) => {
    return async (req, res, next) => {
        if (req.path == "/category") {
            let user = await getUser(req.headers.name);
            if (user.role && user.role === "Admin") {
                if (req.method === 'POST' || req.method === 'PUT') {
                    if (req.body.sub_categories)
                        delete req.body.sub_categories
                } else
                    next();
            } else if (user.role && user.role === "Supervisor") {
                if (req.method != 'GET')
                    res.status(200).json({ status: false, error: response.error.NO_ACCESS });
                else
                    next();
            } else
                res.status(200).json({ status: false, error: response.error.ROLE_NOT_FOUND });
        } else if (req.path == "/category/subCategory") {

        } else if (req.path == "/product") {
            let user = await getUser(req.headers.name);
            if (user.role && user.role === "Admin") {
                if (req.method != 'GET')
                    res.status(200).json({ status: false, error: response.error.NO_ACCESS });
                else
                    next();
            } else if (user.role && user.role === "Supervisor") {
                next();
            } else
                res.status(200).json({ status: false, error: response.error.ROLE_NOT_FOUND });
        } else {
            next();
        }
    }
};

function getUser(name) {
    return new Promise((resolve, reject) => {
        userModel.user.findOne({ name: name, deleted: false }, function (err, user) {
            if (err) {
                reject({ status: false, error: err });
            }
            if (!user) {
                reject({ status: false, error: response.error.INVALID_USERNAME });
            } else {
                resolve(user);
            }
        });
    });
}